# README #


### ArchLinuxArm RPI2 Qt5 hacks ###

You can find in this repository my hacks into the archlinuxarm distro aiming to get the qt5 with eglfs working on the Raspberry PI2 with full multimedia support.

At the time of writing this I was actually able to run a qt5 application without having to go through the whole lousy process of compiling/cross-compiling all of the qt5 libraries.
With just a few hacks to gst-plugins-bad, the eglfs qpa and a patch qt5 multimedia library (which I have yet to upload the pkgbuild and the corresponding patch) I was able to run [QMLCreator](https://github.com/wearyinside/qmlcreator) and corresponding examples on the RPI2.

### If you like to contact me I can be reached at: ###

* <gorilux> grlxapps at gmail dot com